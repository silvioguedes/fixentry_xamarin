using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Android.Text;
using Android.Views.InputMethods;
using Android.Graphics;
using Application = Android.App.Application;
using App;
using App.Droid;
using System.Threading.Tasks;

[assembly: ExportRenderer(typeof(FixEntry), typeof(FixEntryRenderer))]

namespace App.Droid//Change the name of namespace to your project
{
    class FixEntryRenderer : EntryRenderer
    {
        public const int PROBABLY_KEYBOARD_HEIGHT = 100;        
        public static bool showKeyboard = false;
        private static Activity activity = null;
        private static FixEntry fixEntry = null;
        private static Android.Views.View rootView = null;
        private static Rect rect = null;
        private static double entryPercentHeight = 0;

        public static void StartFix(Activity activity_, FixEntry fixEntry_, double entryPercentHeight_)
        {
            activity = activity_;
            fixEntry = fixEntry_;            
            rect = new Rect();
            entryPercentHeight = (double)(100 / entryPercentHeight_);
            
            rootView = activity.Window.DecorView;            
            rootView.ViewTreeObserver.GlobalLayout += OnGlobalLayout;

            var window = ((Activity)Forms.Context).Window;
            window.SetSoftInputMode(SoftInput.StateAlwaysHidden);
            
            Task.Run(() =>
            {
                while (!activity.IsDestroyed)
                {
                    activity.RunOnUiThread(() =>
                    {
                        if (FixEntryRenderer.showKeyboard)
                        {
                            FixEntryRenderer.Utils.ShowKeyboard(activity, null);

                            FixEntryRenderer.showKeyboard = false;
                        }
                    });
                }
            });
        }

        private static void OnGlobalLayout(object sender, EventArgs e)
        {
            rootView.GetWindowVisibleDisplayFrame(rect);
            int screenHeight = rootView.RootView.Height;
            int heightDifference = screenHeight - (rect.Bottom - rect.Top);

            if (fixEntry != null)
            {
                if (heightDifference > PROBABLY_KEYBOARD_HEIGHT)
                {
                    var metrics = activity.Resources.DisplayMetrics;
                    var widthInDp = (double)(metrics.WidthPixels / activity.Resources.DisplayMetrics.Density);
                    var heightInDp = (double)(metrics.HeightPixels / activity.Resources.DisplayMetrics.Density);

                    double h = (double)heightInDp / entryPercentHeight;
                    double w = widthInDp;
                    double y = (double)((rect.Bottom / activity.Resources.DisplayMetrics.Density) - (rect.Top / activity.Resources.DisplayMetrics.Density)) - h;

                    fixEntry.UpdatePosition(fixEntry, Xamarin.Forms.AbsoluteLayout.GetLayoutBounds(fixEntry).X, y, w, h);
                }
                else
                {
                    var metrics = activity.Resources.DisplayMetrics;
                    var widthInDp = (int)(metrics.WidthPixels / activity.Resources.DisplayMetrics.Density);
                    var heightInDp = (int)(metrics.HeightPixels / activity.Resources.DisplayMetrics.Density);

                    double h = (double)heightInDp / entryPercentHeight;
                    double w = widthInDp;
                    double y = (double) heightInDp - h - (rect.Top / activity.Resources.DisplayMetrics.Density);                   

                    fixEntry.UpdatePosition(fixEntry, Xamarin.Forms.AbsoluteLayout.GetLayoutBounds(fixEntry).X, y, w, h);
                }
            }                      
        }
        
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {               
                Control.Touch += (IntentSender, evt) =>
                {
                    new Handler().Post(delegate
                    {
                        if (!showKeyboard)
                        {
                            showKeyboard = true;
                        }
                    });
                };
            }
        }

        public override bool OnInterceptTouchEvent(MotionEvent e)
        {
            Control.RequestFocus();

            return base.OnInterceptTouchEvent(e);
        }

        public static class Utils
        {
            public static void ShowKeyboard(Context context, Android.Views.View view)
            {
                if (view != null)
                {
                    view.RequestFocus();
                }

                InputMethodManager inputMethodManager = context.GetSystemService(Context.InputMethodService) as InputMethodManager;
                inputMethodManager.ShowSoftInput(view, ShowFlags.Forced);
                inputMethodManager.ToggleSoftInput(ShowFlags.Forced, HideSoftInputFlags.ImplicitOnly);
            }

            public static void HideKeyboard(Context context, Android.Views.View view)
            {
                InputMethodManager inputMethodManager = context.GetSystemService(Context.InputMethodService) as InputMethodManager;

                if (view != null)
                {
                    inputMethodManager.HideSoftInputFromWindow(view.WindowToken, HideSoftInputFlags.None);
                }
                else
                {
                    inputMethodManager.HideSoftInputFromWindow(null, HideSoftInputFlags.None);
                }
            }
        }
    }
}