# Usage #

Put the FixEntry.cs in the main folder of your Forms project and FixEntryRenderer.cs in the .Droid folder.

## Code ##

**On Activity.cs:**

FixEntryRenderer.StartFix(this, MainPage.fixEntry, 10);//example using height 10 percent     

**On Page.cs:**

FixEntry fixEntry = new FixEntry();            
fixEntry = fixEntry.CreateFixEntry(layout);//pass the instance of your AbsoluteLayout

**On Page.xaml:**

<AbsoluteLayout x:Name="layout">      
        <ListView x:Name="listView" AbsoluteLayout.LayoutBounds="0.5,0.0,1.0,0.9"  AbsoluteLayout.LayoutFlags="All"/>      
</AbsoluteLayout>
<!-- x = 0.5 -> center in horizontal, y = 0.0 -> starts on top, width = 1.0 -> (100%) full width, height = 0.9 -> 90% of total height-->