﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App//Change the name of namespace to your project
{
    public class FixEntry : Entry
    {                
        public FixEntry CreateFixEntry(AbsoluteLayout layout)
        {
            AbsoluteLayout.SetLayoutFlags(this, AbsoluteLayoutFlags.All);

            layout.Children.Add(this);

            return this;
        }

        public void UpdatePosition(BindableObject bindableObject, double x, double y, double width, double height)
        {
            AbsoluteLayout.SetLayoutBounds(bindableObject, new Rectangle(x, y, width, height));
            AbsoluteLayout.SetLayoutFlags(bindableObject, AbsoluteLayoutFlags.None);
        }
    }    
}
